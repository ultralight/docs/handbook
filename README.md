# Ultralight Handbook

Welcome to the Ultralight Handbook! This document serves as your go-to guide for
understanding our company culture, policies, and procedures. We're building this
for current and prospective employees, stakeholders, and anyone interested in
learning more about how we operate.

---

This handbook is a living document and will be updated periodically to reflect
changes in policies and procedures. We encourage you to provide feedback and ask
questions to ensure it remains a valuable resource for everyone.

For any questions or suggestions, please submit a merge request.

Thank you for being part of Ultralight!

## Previewing The Handbook Locally (In Visual Studio Code)

To ensure that updates and contributions to The Handbook, our company handbook, are accurate and visually appealing, we recommend previewing your changes locally before pushing them to the repository. This section guides you through using Zola and Visual Studio Code's Simple Browser to preview The Handbook on your local machine.

Prerequisites
- Zola Installed: Make sure you have Zola, a fast static site generator, installed on your computer. Zola powers our site's generation.
- Visual Studio Code: You'll need Visual Studio Code with the Simple Browser extension. This setup allows for an integrated previewing experience within your development environment.

### Step 1: Start the Zola Server for The Handbook
- The Handbook is served locally using Zola's built-in server, allowing for real-time updates and previews.
- Open your terminal or command prompt.
- Navigate to The Handbook's project root directory, where the config.toml file resides.

Execute the following command to start the Zola server:
```zsh
zola serve
```

Upon running the command, Zola will initiate a local web server. You'll be provided with a URL, typically http://127.0.0.1:1111, indicating where The Handbook is accessible from your browser.

### Step 2: Preview The Handbook in Visual Studio Code's Simple Browser
With Zola serving The Handbook, use Visual Studio Code's Simple Browser to view your work.

- Launch Visual Studio Code.
- Access the Command Palette by pressing Ctrl+Shift+P (or Cmd+Shift+P on macOS).
- Type and select Simple Browser: Show to initiate the browser.
- When prompted for the URL, enter Zola's serving address (e.g., http://127.0.0.1:1111) and press Enter.

Visual Studio Code's Simple Browser will display The Handbook, allowing you to navigate and review the handbook as if it were live, directly within the IDE.

### Step 3: Iterative Editing and Previewing
As you make changes to The Handbook, Zola will automatically update and regenerate the necessary pages. Simply refresh the Simple Browser view to see your edits. This seamless integration between editing in Visual Studio Code and previewing in Simple Browser streamlines the development and update process for The Handbook.
