+++
title="Engineering Standards"

[extra]
#lead = ""
toc = true
top = false
+++

## Introduction

This document outlines the engineering standards for Ultralight. These standards are designed to help Ultralight engineers work more effectively and efficiently. They cover a range of topics, including Git standards, coding standards, and documentation standards.

## Project Management

![Teamhood](https://img.shields.io/badge/Teamhood-project%20management-blue)

Ultralight uses [Teamhood](https://ultralight.teamhood.com/) for project management. Teamhood is a project management tool that helps teams plan, track, and manage their work. It provides a visual overview of the project, including tasks, timelines, and dependencies.

## Communication Standards

![Slack](https://img.shields.io/badge/Slack-communication-red)

Ultralight uses Slack for team communication. Slack is a messaging platform that allows teams to communicate in real-time. It provides channels for different topics, direct messaging for one-on-one conversations, and integrations with other tools.

### Daily Standup

Team members are expected to post a daily standup message in the `#daily-standups` channel. Standup messages are only required on workdays. The standup message should include the following information:

```plaintext
- What did you work on today?
- What are you planning to work on next?
- Are there any blockers or impediments?
```

## Version Control Standards

![Git](https://img.shields.io/badge/Git-version%20control-orange)
![Git LFS](https://img.shields.io/badge/Git%20LFS-large%20file%20storage-lightgrey)
![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-commit%20message%20standard-yellow)

The team will use Git for version control and GitHub or GitLab for code hosting depending on project needs.

Official Repository Locations:
- GitHub: [https://github.com/ultralightllc](https://github.com/ultralightllc)
- GitLab: [https://gitlab.com/ultralight](https://gitlab.com/ultralight/)

### General Repository Requirements

- All repositories will include a `.gitignore` for all generated files.
- All repositories will include a `README.md` file with a description and operating instructions.
- There will be no source files in the top level directory of any repository.

### Branching Strategy

- The `main` branch will be the default branch for all repositories.
- The `dev` branch will be used for development work and a middle step between feature branches and `main`.
- Feature branches will be created for each contribution. Feature branches will begin with a number to indicate the task number and a short description of the task. Tasks can be found on the [project board](https://ultralight.teamhood.com/).
  - For example, `13-user-authentication` would be a feature branch for task 13, which involves user authentication.
  - Feature branches should be deleted after their changes are merged into `main`.
  - A developer may decide to create working branches off of feature branches for specific tasks. 
    - This is at the discretion of the developer. 
    - Naming conventions for working branches are up to the developer.
    - Working branches should be merged back into the feature branch when the task is complete.
    - Working branches should be deleted after merging into the feature branch.

```mermaid
graph LR
    feature --> dev
    dev --> main
```

### Branch Protection

- The `main` branch will be protected and require code reviews before merging.
- The `dev` branch will be protected and require code reviews before merging.
- Feature branches will not be protected.

### Git LFS

- Large files will be stored using Git LFS (Large File Storage) to prevent repository bloat.
- All binary files, such as images, audio files, and video files, will be stored using Git LFS.
- Developers are responsible for ensuring that large files are stored using Git LFS.
- Developers should not commit large files directly to the repository.

### Commit Message Standards

All commit messages should follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) standard. This standard helps provide a consistent and readable history of changes.

## Appendix

### Tools and Platforms

- **Project Management:**
  - [Teamhood](https://ultralight.teamhood.com/) - Ultralight's chosen project management tool for visual planning, tracking, and management of projects.

- **Communication:**
  - [Slack](https://slack.com/) - Used for real-time team communication, organized by different channels for various topics and direct messaging for private conversations.

- **Version Control:**
  - [Git](https://git-scm.com/) - The version control system used for tracking changes in the source code during software development.
  - [Git LFS (Large File Storage)](https://git-lfs.github.com/) - Used for handling large files to prevent repository bloat.

- **Coding Standards:**
  - [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) - A specification for adding human and machine-readable meaning to commit messages.