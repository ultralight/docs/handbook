+++
title="Issue Templates"

[extra]
#lead = ""
toc = true
top = false
+++

## User Stories

TITLE: [Short, descriptive title for the user story]

As a [type of user],
I want [the goal or the action to be performed],
So that [the benefit or value derived from the action].


**Acceptance Criteria:**

1. Given [the initial context or state], When [the action taken], Then [the expected outcome or result].
2. Given [another initial context or state], When [another action taken], Then [another expected outcome or result].
3. Given [another initial context or state], When [another action taken], Then [another expected outcome or result].
4. Given [another initial context or state], When [another action taken], Then [another expected outcome or result].


**Attachments:**

Wireframes, mockups, diagrams, or any relevant files.


**Discussion:**

[Summary of key points from team discussions, stakeholder feedback, or playtesting insights.]


**Ultralight's Standards and Compliance**
All implementations must adhere to our organization’s standards and expectations. For detailed guidelines, please refer to the following:
- [Design Principles](@/product/design/principles.md) - Ensure all designs align with these principles.
- [Engineering Standards](@/product/engineering/standards.md) - Follow these standards for any development work.