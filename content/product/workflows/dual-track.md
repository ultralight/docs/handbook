+++
title="Dual-Track Workflow"
toc = true

[extra]
#lead = ""
toc = true
top = false
+++

As our team evolves and our projects become more complex, we've recognized the need for a more structured approach to our workflow. Inspired by the [Product Development Flow](https://handbook.gitlab.com/handbook/product-development-flow/) at GitLab (to whom we owe a big thank you! ❤️), we're excited to introduce our own Dual-Track Workflow.

#### Why a Dual-Track Workflow?

Our journey began with a simple, single-track workflow that suited us well when our team was just one or two members strong. However, as we've grown and our ambitions have expanded, the limitations of this approach have become clear. We needed a system that could help us decouple our design and development work stages so we could focus independently while still promoting a high level of collaboration.

It's important to note that we're not simply copying GitLab's model. What works for a large organization like GitLab might not work for us. Our goal is to take the principles that resonate with us and adapt them to our unique context. This means implementing changes gradually, ensuring that each new element of the workflow genuinely benefits our processes.

This introduction is just the beginning. We plan to document and refine our workflow as we go, adding more detail and adjusting based on our experiences. We're committed to creating a workflow that not only meets our current needs but is flexible enough to grow with us.

## Track 1: Validation

This first track allows the team to focus on design and looking deep into the customer's problem space. We don't necessarily need to commit to the intense work of constructing a polished, well-engineered software (or hardware) solution at this stage. Instead, the work done in this track is all about ensuring that we're solving the right problem and building the right thing.

## Track 2: Development

This track is dedicated to the actual construction and development of the solution. The work in this phase is based on what we've learned from Track 1, and its goal is to bring our proposed solution to life in the most efficient, high-quality way possible.

## A Few Notes on Kanban Board Usage

For the uninitiated, there's a brief and fun introduction to kanban on
[YouTube]( https://www.youtube.com/watch?v=LFYnkFq3ITE). We switched to kanban
after trying to adhere to scrum for a long time, but we ultimately weren't
thrilled with the lack of flexibility. For starters, we had people spread across multiple time zones and some people were affected by connectivity challenges. Not being able to be consistently in the same room or get everyone into a regularly scheduled video call made scrum quite challenging. That's what triggered our switch to kanban and seeking an async-first workflow in general as well.

What we have now is more flexibility about when
planning and review happens. We still maintain regular meetings on the books to
connect and discuss how our work is going and what we can do better, but the
meetings are no longer dedicated to planning, or dictating our pace of work.

**Reserved Color Code Statuses**

- **Gray**: Gray signifies a non-value adding state for issues. Items in gray
status columns, such as the *Needs Design* status column are basically in a
parking lot after having been pushed there (placed there by someone) upon the
conclusion of some prior step. __Items in gray columns should have no assignees,
but they should be prioritized so that the most important items are on top.__
When capacity to do work on the item becomes available, a team member "binds"
(self-assigns or is assigned) to the item and pulls it into the next state. For
example, from *Needs Design* (no assignee, no value being created) to *In
Design* (someone is now creating value with that item).