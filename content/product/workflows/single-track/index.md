+++
title = "Single-Track Workflow"
weight = 1
sort_by = "weight"

[extra]
lead = "This page describes our current workflow, however, you're encouraged to develop a dual-track workflow that separates product design and validation cycles from implementation."
toc = true
top = false
+++

This workflow will be deprecated in favor of the [dual-track workflow](/product/workflows/dual-track).

## Concept Stage

Great ideas can come from anywhere, and story mapping is a powerful tool to identify and organize them. It is a collaborative process that enables the team to visualize and break down the work required to achieve a desired outcome.

**Story Mapping as a Game Studio**

For a small game studio, story mapping is an invaluable technique for visually organizing and prioritizing narrative and gameplay elements. It allows us to deconstruct our game's overarching story and features into manageable segments, aligning them with the player's journey to ensure a cohesive experience. By mapping out main story arcs, quests, character development, and game mechanics, we can easily see how each component fits into the larger narrative, identify any gaps or overlaps, and adjust our development priorities accordingly. Story mapping fosters a player-centric approach, ensuring that we create engaging and memorable experiences while efficiently managing our development resources.

![Story Map Structure](story-map-structure.png "Story Map Structure")

This example image illustrates how story maps are structured. This is where an idea is initially expressed, shared with the team, and organized to see how it integrates into the bigger picture.

## Input / Requirements Stage

As space in the Input/Requirements Stage becomes available, we pull high priority items from the story map.

Activities Include:
- Gathering Requirements: Clarifying what needs to be done, gathering detailed requirements from stakeholders, and documenting initial specifications.

New Status:
- This is a "new" item, probably pulled from the story map and placed into this status by the product owner or product manager.

Issues Identified / Bugs Reported:
- When testers identify issues, they move the card to this column and attach detailed bug reports. This signals the development team to address these issues. Each bug can also be tracked as a sub-task or linked issue within the card.

Ready Status:
- Requirements are clearly defined, ex:
	- Learning objectives (if any) - outlined with a plan to test/measure educational impact
	- Narrative concepts
	- Input/Output-Feedback Expectations
- Background context is clear.
- Dependencies are resolved.
- All necessary information is documented.
- The team agrees the item is ready to be worked on.

## Design & Prototyping Stage

Design Status:
- The team has started to design the feature or user story,
	- ex: brainstorming, wire-framing, concept art, pseudo-code
- (Optional) Task To do/in-progress/done Substatuses: These can track work from ideation to refinement, to clickable mockup (prototype).

Review Status:
- Task-flows, mockups and/or prototypes, complete and ready for stakeholder feedback
- Placeholder/temp sound or music added (if required)
- Concept/placeholder art (if required) has been completed

Ready for Implementation Status:
- The team has discussed the design
- (Experimental: Stakeholder feedback has been captured)
- Any necessary refinements have been made
- The design has sufficient documentation for clean implementation and testing, ex:
	- pass/fail criteria for testing identified
- The team agrees the item is ready for implementation

## Implementation Stage

Build Status:
- The team has started implementing the design, potentially also including revised art, sound, and animation.

Ready for Testing:
- The feature has been implemented as per the design doc
- The build is green (passing) in the dev branch
- (if applicable):
	- updated art, music, and sound has been added
	- automated unit tests and/or integration tests have been implemented

## QA & Testing Stage

The QA & Testing Stage is critical for ensuring the quality and playability of the game. At this point, the game or feature undergoes rigorous testing to identify and fix any issues or bugs, ensuring it meets our high standards for quality and player experience.

Activities Include:
- Functional Testing: Verifying that game mechanics, controls, and features work according to the design documents and specifications.
- Usability Testing: Ensuring the game is intuitive, enjoyable, and accessible to our target audience. This may involve playtesting sessions with players from our target demographic to gather feedback on the game's usability and fun factor.
- Performance Testing: Checking the game’s performance on different platforms and configurations to ensure smooth gameplay without lag or crashes.
- Compatibility Testing: Ensuring the game runs as expected on various devices, operating systems, and screen resolutions.
- Regression Testing: After fixes or updates, re-testing to ensure that previous aspects of the game still function correctly and new changes haven't introduced new issues.

QA in Progress Status:
- Ready for Release Status: Cards that pass all tests without any outstanding issues are moved to this final column in the QA & Testing Stage. It signifies that the game or feature has met all quality benchmarks and is ready for launch or delivery to players.
